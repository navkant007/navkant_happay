from django.shortcuts import render
from .models import Studentdata
# Create your views here.
from django.utils.datastructures import MultiValueDictKeyError
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.http import QueryDict
from django.views.generic import View
from django.http import HttpResponse
from TestSum.models import Studentdata
class Student(View):
    def get(self, request):
        answer = []
        stud_val = Studentdata.objects.all()
        for i in stud_val:
            ans = {}
            ans['fname'] = i.fname
            ans['lname'] = i.lname
            ans['rollno'] = i.rollno
            ans['address']= i.address
            answer.append(ans)
            #<html><br></html>
        return HttpResponse(answer, status = 200)
class Insert(View):
    def post(self, request):
        try:
            body = request.POST
            fname1 = body['fname']
            lname1 = body['lname']
            rollno1 = body['rollno']
            address1 = body['address']
            navi = Studentdata(fname = fname1, lname = lname1, rollno = rollno1 , address = address1)
            #import pdb;pdb.set_trace();
            navi.save()
            return HttpResponse("successfully data inserted !" , status = 200)
        except IntegrityError:
            return HttpResponse("Roll No already Exist" , status = 500)
        except MultiValueDictKeyError:
            return HttpResponse("Multi Value Dict Key Error" , status = 600)
class Delete(View):
    def delete(self, request, id):
        try:
            #dlt = request.Delete
            #rollno1 = dlt['rollno']
            navi = Studentdata.objects.get(id = id)
            navi.delete()
            return HttpResponse(request , status = 200)
        except ObjectDoesNotExist:
            return HttpResponse("ID Does not Exist")
class Update(View):
    def put(self, request, rollno):
        try:
            """
            update1 = request.POST
            rollno1 = update1['rollno']
            fname1 = update1['fname']
            lname1 = update1['lname']
            uprollno1 = update1['uprollno'] #if u want to update your primary key
            address1 = update1['address']
            navi = Studentdata.objects.filter(rollno = rollno1)
            """
            req = QueryDict(request.body)
            obj = Studentdata.objects.get(rollno = rollno)
            if 'fname' in req.keys():
                obj.fname = req['fname']
            if 'lname' in req.keys():
                obj.lname = req['lname']
            if 'rollno' in req.keys():
                obj.rollno = req['rollno']
            if 'address' in req.keys():
                obj.address = req['address']
            obj.save()
            #navi.update(fname = fname1, lname = lname1, rollno = uprollno1, address = address1)
            return HttpResponse("Successfully updated the database")
        except ObjectDoesNotExist:
            return HttpResponse(" Object Does not Exist")


"""
    def get(self, request):
        try:
            p1 = request.GET['p1']
            p2 = request.GET['p2']
            result = int(p1) + int(p2)
            return HttpResponse('sum is : ' + str(result), status=200)
        except Exception as exception:
            print str(exception)
            return HttpResponse("Internal Server Error", status=500)
"""
