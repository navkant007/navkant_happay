# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TestSum', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Studentdata',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fname', models.CharField(max_length=15)),
                ('lname', models.CharField(max_length=15)),
                ('rollno', models.CharField(unique=1, max_length=15)),
                ('address', models.CharField(max_length=15)),
            ],
        ),
        migrations.DeleteModel(
            name='Student',
        ),
    ]
