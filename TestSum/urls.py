from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from views import TestSum
from TestSum.views import Studentdata


urlpatterns = [
	url(r'^sum/$', csrf_exempt(Studentdata.as_view()),
		name="test-sum"),]
