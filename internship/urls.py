"""internship URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include, url
from TestSum.views import Student
from TestSum.views import Insert
from TestSum.views import Delete
from TestSum.views import Update

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^test/', Student.as_view(),name = 's'),
    url(r'^insert/', Insert.as_view(),name = 's'),
    url(r'^delete/(?P<id>\d+)', Delete.as_view(),name = 's'),
    url(r'^update/(?P<rollno>\d+)', Update.as_view(),name = 's'),
]
