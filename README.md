Introduction
          This project contains assignments on Django & Python Basics.It gives basic ideas of api and database connectivity.
          The main goal of this project is to learn basic Django and implement it.Request has been implemented such as GET,POST
          PUT,DELETE.
          This project basically use SQLite3 Database.
          There are different classes and methods which are implemented for deletion,  creation, fetching information, updation.

      
          Installation Process
      	    	To run the code there are some  prerequisite 
            	1-Install Python 2.7
            	2-Install Django 1.8
			i) To install Django we need to create the Virtual Environment 
				a)("sudo install pip")
				b)("pip install virtualenv") (now enter the virtual environment)
				c)("pip install django==1.8")
            	3-Install Postman


           To Run the Project
		1-First of all we need to go to Virtual Environment and run commend on terminal("source bin/activate") now you are in the Virtual Environment  
            	2-To run the code just go to the directory where the code folder is there . 
            	3-Run Server by using command ("python manage.py runserver")
            	4-To run operation there are methods used to request such as get, post, put ,delete. Now there are urls provided for certain operations.s
		5-Just run these Queries on the Postman 
            		Fetch Entries from database : http://127.0.0.1:8000/test/
            		Create Entries :http://127.0.0.1:8000/insert/                                   
            		Delete Entries : http://127.0.0.1:8000/delete/id.                              [id is the id no in the database ]
            		Update Record : http://127.0.0.1:8000/update/rollno                            [rollno is the  rollnumber in the database]

            Create records, delete records and update records will be done using postman.